// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import { commands, CompletionItem, ExtensionContext, languages, SnippetString, window } from 'vscode';
import { ProjectIndexer } from './tools/projectIndexer';
import { ClassObject } from './tools/classObject';
import { ProjectCompiler } from './tools/projectCompiler';

/**
 * This is the array of all Eiffel keywords (used for auto completion provider)
 */
const langageKeyWords = ["Indexing", "indexing", "note", "deferred",
	"expanded", "class", "inherit", "rename", "as", "attached", "export", "undefine",
	"redefine", "select", "all", "create", "creation", "feature -- ", "feature {NONE} -- ", "prefix",
	"infix", "separate", "frozen", "local", "is", "unique", "once", "external", "alias", "require", "ensure", "invariant",
	"variant", "rescue", "retry", "like", "check", "else", "elseif",
	"then", "inspect", "when", "across", "from", "loop", "until", "debug", "not", "or",
	"and", "xor", "implies", "old", "end", "result"];

/**
 * The eiffel compiler path
 */
let ecPath: string;

/**
 * The current working directory
 */
let cwdPath: string;

/**
 * The .ecf file name of the project
 */
let projectFileName: string;

/**
 * Below is the checks if environment variables have been set for the above three attributes "ecPath", "cwdPath" and "projectFileName".
 */
// Check for Eiffel compiler path
if (process.env["EIFFEL_VSCODE_EC_PATH"]) {
	ecPath = process.env["EIFFEL_VSCODE_EC_PATH"];
} else {
	ecPath = "";
	window.showInformationMessage
	('You must create and set the environnement variable EIFFEL_VSCODE_EC_PATH with your Eiffel compiler path.');
}
// Check for Eiffel project directory path
if (process.env["EIFFEL_VSCODE_PROJECT_DIR_PATH"]) {
	cwdPath = process.env["EIFFEL_VSCODE_PROJECT_DIR_PATH"];
} else {
	cwdPath = "";
	window.showInformationMessage
	('You must create and set the environnement variable EIFFEL_VSCODE_PROJECT_DIR_PATH with your Eiffel project directory path.');
}
// Check for Eiffel project file name
if (process.env["EIFFEL_VSCODE_PROJECT_FILE_NAME"]) {
	projectFileName = process.env["EIFFEL_VSCODE_PROJECT_FILE_NAME"];
} else {
	projectFileName = "";
	window.showInformationMessage
	('You must create and set the environnement variable EIFFEL_VSCODE_PROJECT_FILE_NAME with your Eiffel project file name. Example : my_awesome_eiffel_project.ecf.');
}

//Initialization of the indexer (which use "ec", the Eiffel compiler).
const projectIndexer = new ProjectIndexer(
	ecPath,
	cwdPath,
	projectFileName);
// Launch a first indexing when opening the project
projectIndexer.indexProject();

// Initialization of the compiler.
const projectCompiler = new ProjectCompiler(
	ecPath,
	cwdPath,
	projectFileName);

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "eiffel-vscode" is now active!');

	// Define the command to compile and run the project
	const compileAndRunCommand = commands.registerCommand('eiffel-vscode.compile-and-run', async () => {
		await projectCompiler.compileAndRun();
	});
	// Register the command
	context.subscriptions.push(compileAndRunCommand);

	// Define the command to index the project
	const indexProjectCommand = commands.registerCommand('eiffel-vscode.index-project', async () => {
		await projectIndexer.indexProject();
	});
	// Register the command
	context.subscriptions.push(indexProjectCommand);

	// Example of command for hover content (not used at the moment, uncomment if needed)
	// const hoverDisposable = languages.registerHoverProvider('eiffel', {
	// 	provideHover(document, position, token) {
	// 		return {
	// 			contents: ['Hover Content']
	// 		};
	// 	}
	// });
	// context.subscriptions.push(hoverDisposable);

	// Register the command to provide auto completion for Eiffel language keywords
	const langageKeyWordsProvider = languages.registerCompletionItemProvider({ scheme: "file", language: "eiffel" },
		{
			provideCompletionItems(document, position) {
				const completionItems: CompletionItem[] = Array();
				langageKeyWords.forEach(langageKeyWord => {
					completionItems.push(new CompletionItem(langageKeyWord));
				});
				return completionItems;
			}
		});
	// Register the command
	context.subscriptions.push(langageKeyWordsProvider);

	// Define the command to provide auto completion for classes of the project
	const autoCompletionForClassNames = languages.registerCompletionItemProvider({
		scheme: "file",
		language: "eiffel"
	}, {
		provideCompletionItems(document, position) {
			const completionItems: CompletionItem[] = Array();
			projectIndexer.classes.forEach(classObject => {
				completionItems.push(new CompletionItem(classObject.name));
			});
			return completionItems;
		}
	});
	// Register the command
	context.subscriptions.push(autoCompletionForClassNames);

	/** 
	 * Define the command to provide auto completion for routines and attributes of an object instance
	 * of the current document (triggered by a "." character)
	 * 
	 * The method will first get the class of the current file you're in, then it will check for the attributes
	 * and routine of this class. Then, it will check if you typed a "." character right after one of these attributes.
	 * If so, it will go get the type (class) of the object and provide the according auto completion for it.
	 * 
	 */
	const autoCompletionForClassRoutinesAndAttributes = languages.registerCompletionItemProvider({
		scheme: "file",
		language: "eiffel"
	}, {
		provideCompletionItems(document, position) {
			const completionItems: CompletionItem[] = Array();
			const currentDocumentClassNameRegExp = new RegExp("(?<=^([^\"]|\"[^\"]*\")*)\\bclass\\b\\s*(\\b.*\\b)", "gi");
			const documentText = document.getText();
			const matchsCurrentDocumentClassNameRegExp = currentDocumentClassNameRegExp.exec(documentText);
			if (matchsCurrentDocumentClassNameRegExp) {
				const currentClassName = matchsCurrentDocumentClassNameRegExp[2];
				const classObject = projectIndexer.classes.find((element) => {
					return element.name === currentClassName;
				});
				if (classObject) {

					const linePrefix = document.lineAt(position).text.substring(0, position.character - 1).trim();
					const classNameToProvideAutoCompletion = isAttributeOrConstant(linePrefix, classObject);

					if (classNameToProvideAutoCompletion) {
						//Add attributes, constants and routines for auto completion
						const classObjectToProvideAutoCompletion = projectIndexer.classes.find((element) => {
							return element.name === classNameToProvideAutoCompletion;
						});
						if (classObjectToProvideAutoCompletion) {
							classObjectToProvideAutoCompletion.attributes.forEach(classAttribute => {
								completionItems.push(new CompletionItem(classAttribute.name));
							});
							classObjectToProvideAutoCompletion.constants.forEach(classConstant => {
								completionItems.push(new CompletionItem(classConstant.name));
							});
							classObjectToProvideAutoCompletion.routines.forEach(classRoutine => {
								const completionItem = new CompletionItem(classRoutine.name);
								const snippet = new SnippetString(classRoutine.snippetString);
								completionItem.insertText = snippet;
								completionItems.push(completionItem);
							});
						}
						return completionItems;
					}
				}
			}
			return undefined;
		}
	}, '.');
	// Register the command
	context.subscriptions.push(autoCompletionForClassRoutinesAndAttributes);

	/**
	 * Define the snippet command to provide auto completion for documentation of a routine.
	 * This auto completion is accessible by the "Routine documentation" name.
	 * 
	 * Note that the routine documentation must be on a single line and you must 
	 * call the snippet right under it for the snippet to work properly.
	 * 
	 * Also note that this auto completion is not mentionned specifically in the README as it is not
	 * really conventionnal in the Eiffel language.
	 */
	const autoCompletionDocumentationFrameProvider = languages.registerCompletionItemProvider({
		scheme: "file",
		language: "eiffel"
	}, {
		provideCompletionItems(document, position) {
			const completionItem = new CompletionItem("Routine documentation");
			completionItem.documentation = "Routine documentation. The routine documentation must be" +
				" on a single line and you must call the snippet right under it for the snippet to work properly.";
			completionItem.keepWhitespace = false;
			const methodDeclarationLine = document.lineAt(position.line - 1);
			const firstNonWhitespaceCharacterIndex = methodDeclarationLine.firstNonWhitespaceCharacterIndex;
			const methodDeclarationLineText = methodDeclarationLine.text.substring(firstNonWhitespaceCharacterIndex);
			const indexFirstOpeningParenthesis = methodDeclarationLineText.indexOf("\(");
			const indexLastClosingParenthesis = methodDeclarationLineText.lastIndexOf("\)");
			const methodArgumentsByType = methodDeclarationLineText.substring(indexFirstOpeningParenthesis + 1, indexLastClosingParenthesis).split(";");

			let methodArgumentsWithoutType: string[] = [];
			methodArgumentsByType.forEach(element => {
				methodArgumentsWithoutType.push(element.split(":")[0].trim());
			});

			let methodArguments: string[] = [];
			methodArgumentsWithoutType.forEach(element => {
				element.split(",").forEach(e => {
					methodArguments.push("\`" + e.trim() + "\`");
				});
			});

			let snippetString = "-- ${1:Comments for a feature.}\n-- ";
			for (let index = 0; index < methodArguments.length; index++) {
				const element = methodArguments[index];
				snippetString = snippetString + `\n-- ${element}: $${index + 2}`;
			}
			//Check if the routine is a function and the "Result" statement if needed.
			if (methodDeclarationLine.text.substring(indexLastClosingParenthesis).includes(":")) {
				snippetString = snippetString + `\n-- \`Result\`: $\{${methodArguments.length + 2}:The function result\}`;
			}

			const snippet = new SnippetString(snippetString);
			completionItem.insertText = snippet;

			return [
				completionItem
			];
		}
	});
	// Register the command
	context.subscriptions.push(autoCompletionDocumentationFrameProvider);

}

// This method is called when your extension is deactivated
export function deactivate() { }

/**
 * Check is the line prefix is an attribute or a constant from the specified class and return
 * the type of the matched attribute (or constant) class name or undefined if nothing has been found.
 * 
 * @param linePrefix The line prefix
 * @param classObject The specified class
 */
const isAttributeOrConstant = function isAttributeOrConstantFromClass(linePrefix: string, classObject: ClassObject) {
	let validAttributOrConstant = false;
	let classNameToProvideAutoCompletion = undefined;

	let j = 0;
	while (!validAttributOrConstant && j < classObject.attributes.length) {
		let attributeName = classObject.attributes[j].name;
		const attributeNameRegExp = new RegExp(`\\b${attributeName}\\b`, "gi");
		const matchsAttributeNameRegExp = attributeNameRegExp.exec(linePrefix);
		if (matchsAttributeNameRegExp) {
			classNameToProvideAutoCompletion = classObject.attributes[j].type;
			validAttributOrConstant = true;
		}
		j++;
	}
	j = 0;
	while (!validAttributOrConstant && j < classObject.constants.length) {
		let constantName = classObject.constants[j].name;
		const constantNameRegExp = new RegExp(`\\b${constantName}\\b`, "gi");
		const matchsConstantNameRegExp = constantNameRegExp.exec(linePrefix);
		if (matchsConstantNameRegExp) {
			classNameToProvideAutoCompletion = classObject.constants[j].type;
			validAttributOrConstant = true;
		}
		j++;
	}
	return classNameToProvideAutoCompletion;
};

