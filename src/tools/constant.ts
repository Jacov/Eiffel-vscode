/**
 * Class that represents a constant from a class of the Eiffel project
 */
export class Constant {
	name: string = "";
	type: string = "";
}