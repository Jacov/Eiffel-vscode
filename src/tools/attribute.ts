/**
 * Class that represents an attribute from a class of the Eiffel project
 */
export class Attribute {
	name: string = "";
	type: string = "";
}