import { ClassObject } from './classObject';
import { Attribute } from './attribute';
import { Routine } from './routine';
import { Constant } from './constant';
import { CompilerLauncher } from './compilerLauncher';
import { CompletionItem, Progress } from 'vscode';

/**
 * Class for the indexing of the project
 */
export class ProjectIndexer extends CompilerLauncher {

    /**
     * The classes of the project
     */
    public classes: ClassObject[];

    /**
     * Constructor
     * 
     * @param ecbPath The path of the eiffel compiler
     * @param cwd The current working directory
     * @param projectFileName The project file name
     */
    constructor(ecbPath: string, cwd: string, projectFileName: string) {
        super("Eiffel indexer", ecbPath, cwd, projectFileName);
        this.classes = [];
    }


    /**
     * Intialize the indexing of the project
     * 
     * Compile the project, get the classes, start the progress bar process, 
     * get the identifiers and then end the progress bar process.
     */
    public async indexProject(): Promise<void> {
        try {
            this._extensionOutputChannel.show();
            this._extensionOutputChannel.appendLine("Start indexing...");
            await this._compileProject();
            console.log("Compilation of the project done.");
            this._extensionOutputChannel.appendLine("Compilation of the project done.");

            await this._indexClassesForAutoCompletion();
            console.log("Initialization of classes done.");
            this._extensionOutputChannel.appendLine("Initialization of classes done.");

            await this._startProgressBarProcess(this.classes.length, "Indexing project ");

            let i = 0;
            while (i < this.classes.length) {
                const classObject = this.classes[i];
                await this._indexClassAttributesForAutoCompletion(classObject);
                await this._indexClassConstantsForAutoCompletion(classObject);
                await this._indexClassRoutinesForAutoCompletion(classObject);
                i++;
                this._progessionIndex = i;
            }
            console.log("Initialization of attributes, constants and routines done.");
            this._extensionOutputChannel.appendLine("Initialization of attributes, constants and routines done.");
        } catch (error: any) {
            console.log(`Error while initializing the project. Details : ${error}`);
            this._extensionOutputChannel.appendLine(`Error while initializing the project. Details : ${error}`);
            throw new Error(error);
        }
    }


    /**
     * Compile the project for class and identifiers data access
     */
    private async _compileProject(): Promise<void> {
        try {
            await this._executeEcbCommandAsync("-batch -stop", []);
        } catch (error: any) {
            console.log(`Error while compiling the project. Details : ${error}`);
            throw new Error(error);
        }
    }

    /**
     * Get the classes (from the project) and store them for auto-completion purpose.
     */
    private async _indexClassesForAutoCompletion(): Promise<void> {
        try {
            this.classes = [];
            const classNamesdata = await this._executeEcbCommandAsync("-batch -stop -loop", ["M", "S", "L", "Q"]);
            classNamesdata.split("\n").forEach(classLine => {
                let lClassLine = classLine || "";
                lClassLine = lClassLine.trim();
                if (lClassLine.length > 0 && lClassLine.indexOf("\(") < 0) {
                    const lineIsClassDeclarationRegExp = new RegExp("([^\\*]*)\\*?", "g");
                    const matchsLineIsClassDeclarationRegExp = lineIsClassDeclarationRegExp.exec(lClassLine);
                    if (matchsLineIsClassDeclarationRegExp) {
                        const classObject = new ClassObject();
                        classObject.name = matchsLineIsClassDeclarationRegExp[1];
                        this.classes.push(classObject);
                    }
                }
            });
        } catch (error: any) {
            console.log(`Error while initializing classes data. Details : ${error}`);
            throw new Error(error);
        }
    }

    /**
     * Get the attributes of a class (from the project) and store them for auto-completion purpose.
     * 
     * @param classObject The class to get the attributes from
     */
    private async _indexClassAttributesForAutoCompletion(classObject: ClassObject): Promise<void> {
        try {
            const classAttributesdata = await this._executeEcbCommandAsync("-batch -stop -loop", ["M", "C", "B", classObject.name, "", "Q"]);
            classAttributesdata.split("\n").forEach(attributeLine => {
                let lAttributeLine = attributeLine || "";
                lAttributeLine = lAttributeLine.trim();
                if (lAttributeLine.length > 0) {
                    const lineStartsWithClassRegExp = new RegExp("^\\s*\\bclass\\b", "gi");
                    const matchsLineStartsWithClassRegExp = lineStartsWithClassRegExp.exec(lAttributeLine);
                    //There are few cases where the compiler returns something like "Set_callback_pointers" as a constant line.
                    //To prevent saving something like this in the classObject, we add the undexOf(":") verification.
                    if (!matchsLineStartsWithClassRegExp && lAttributeLine.indexOf(":") >= 0) {
                        const attributeArrayData = lAttributeLine.split(":");
                        const attribute = new Attribute();
                        attribute.name = attributeArrayData[0].trim();
                        attribute.type = attributeArrayData[1].trim();
                        classObject.attributes.push(attribute);
                    }
                }
            });
        } catch (error: any) {
            console.log(`Error while initializing attributes data of ${classObject.name}. Details : ${error}`);
            throw new Error(error);
        }
    }

    /**
     * Get the constants of a class (from the project) and store them for auto-completion purpose.
     * 
     * @param classObject The class to get the constants from
     */
    private async _indexClassConstantsForAutoCompletion(classObject: ClassObject): Promise<void> {
        try {
            const classConstantsdata = await this._executeEcbCommandAsync("-batch -stop -loop", ["M", "C", "O", classObject.name, "", "Q"]);
            classConstantsdata.split("\n").forEach(constantLine => {
                let lConstantLine = constantLine || "";
                lConstantLine = lConstantLine.trim();
                if (lConstantLine.length > 0) {
                    const lineStartsWithClassRegExp = new RegExp("^\\s*\\bclass\\b", "gi");
                    const matchsLineStartsWithClassRegExp = lineStartsWithClassRegExp.exec(lConstantLine);
                    //There are few cases where the compiler returns something like "Set_callback_pointers" as a constant line.
                    //To prevent saving something like this in the classObject, we add the undexOf(":") verification.
                    if (!matchsLineStartsWithClassRegExp && lConstantLine.indexOf(":") >= 0) {
                        const constantArrayData = lConstantLine.split(":");
                        const constant = new Constant();
                        constant.name = constantArrayData[0].trim();
                        constant.type = constantArrayData[1].trim().split(" ")[0];
                        classObject.constants.push(constant);
                    }
                }
            });
        } catch (error: any) {
            console.log(`Error while initializing constants data of ${classObject.name}. Details : ${error}`);
            throw new Error(error);
        }
    }

    /**
     * Get the routines of a class (from the project) and store them for auto-completion purpose.
     * 
     * @param classObject The class to get the routines from
     */
    private async _indexClassRoutinesForAutoCompletion(classObject: ClassObject): Promise<void> {
        try {
            const classRoutinesdata = await this._executeEcbCommandAsync("-batch -stop -loop", ["M", "C", "R", classObject.name, "", "Q"]);
            classRoutinesdata.split("\n").forEach(classRoutineDataLine => {
                let lClassRoutineDataLine = classRoutineDataLine || "";
                lClassRoutineDataLine = lClassRoutineDataLine.trim();
                if (lClassRoutineDataLine.length > 0 && lClassRoutineDataLine.indexOf("Class") < 0) {
                    const routineBaseRegExp = new RegExp("^([^\\(\\:]*[^\\(\\:])", "g");
                    const routineHasArgumentsRegExp = new RegExp("^(.*)\\((.*)\\)", "g");
                    const routineHasReturnRegExp = new RegExp("^(.*)(\\(.*\\))\\:(.*)", "g");
                    const matchsBaseRegex = routineBaseRegExp.exec(lClassRoutineDataLine);
                    const matchsHasArgumentsRegex = routineHasArgumentsRegExp.exec(lClassRoutineDataLine);
                    const matchsHasReturnRegex = routineHasReturnRegExp.exec(lClassRoutineDataLine);
                    const completionItem = new CompletionItem("");
                    const routine = new Routine();
                    let lSnippetString = "";
                    if (matchsBaseRegex) {
                        routine.name = matchsBaseRegex[1].trim();
                        completionItem.label = routine.name;
                        lSnippetString = `${completionItem.label}`;
                        if (matchsHasArgumentsRegex) {
                            lSnippetString = lSnippetString + "\(";
                            const argumentsArray = matchsHasArgumentsRegex[2].split(";");
                            for (let index = 0; index < argumentsArray.length; index++) {
                                const argumentElement = argumentsArray[index];
                                routine.arguments.push(argumentElement.trim());
                                lSnippetString = lSnippetString + `$\{${index + 1}:${argumentElement}\}`;
                                if (index < argumentsArray.length - 1) {
                                    lSnippetString = lSnippetString + ", ";
                                }
                            }
                            lSnippetString = lSnippetString + "\)";
                        }
                        if (matchsHasReturnRegex) {
                            routine.return = matchsHasReturnRegex[3].trim();
                        }
                    }
                    routine.snippetString = lSnippetString;
                    classObject.routines.push(routine);
                }
            });
        }
        catch (error: any) {
            console.log(`Error while initializing routines data of ${classObject.name}. Details : ${error}`);
            throw new Error(error);
        }
    }

    protected override _updateProgressBar(progress: Progress<{
        message?: string | undefined;
        increment?: number | undefined;
    }>, maxProgressIndex: number): void {
        const lProgessionIndex = this._progessionIndex;
        const lMaxProgressIndex = maxProgressIndex;
        const lIncrement = (100 * (lProgessionIndex - this._oldProgressionIndex)) / lMaxProgressIndex;
        const currentClassNameIndexing = this?.classes[lProgessionIndex]?.name || "";
        progress.report({ increment: lIncrement, message: `currently indexing ${currentClassNameIndexing}...` });
        this._oldProgressionIndex = lProgessionIndex;
        if (lProgessionIndex < lMaxProgressIndex) {
            setTimeout(() => {
                this._updateProgressBar(progress, maxProgressIndex);
            }, 1000);
        }
    }
}