import { exec } from 'child_process';
import { OutputChannel, Progress, ProgressLocation, window } from 'vscode';

/**
 * Abstract class for classes that have a progress bar process and uses the "exec" method
 */
export abstract class CompilerLauncher {

     /**
      * If an error happened during the process (for the progress bar to be stopped)
      */
     public _error: boolean;

     /**
     * The output channel name
     */
     protected _outPutChannelName: string;

     /**
     * The eiffel compiler path
     */
     protected _ecbPath: string;

     /**
      * The current working directory
      */
     protected _cwd: string;

     /**
      * The .ecf file name of the project
      */
     protected _projectFileName: string;

     /**
      * The old progression index of the indexing job.
      */
     protected _oldProgressionIndex: number;

     /**
      * The progression index of the indexing job.
      */
     protected _progessionIndex: number;

     /**
      * The output channel of the extension (visible for the user)
      */
     protected _extensionOutputChannel: OutputChannel;

     /**
      * Constructor
      * 
      * @param outPutChannelName The output channel name
      * @param ecbPath The path of the eiffel compiler
      * @param cwd The current working directory
      * @param projectFileName The project file name
      */
     constructor(outPutChannelName: string, ecbPath: string, cwd: string, projectFileName: string) {
          this._error = false;
          this._outPutChannelName = outPutChannelName;
          this._ecbPath = ecbPath;
          this._cwd = cwd;
          this._projectFileName = projectFileName;
          this._oldProgressionIndex = 0;
          this._progessionIndex = 0;
          this._extensionOutputChannel = window.createOutputChannel(this._outPutChannelName);
     }

     /**
     * Execute an ecb command (eiffel compiler)
     * 
     * @param additionnalArgs Additionnal arguments to be passed to the command
     * @param inputs An array of each user commands to be exectued before trying to get the output. Example : ["S", "L", "Q"].
     * @returns The output string from the error console (stderr)
     */
     protected async _executeEcbCommandAsync(additionnalArgs: string, inputs: string[]): Promise<string> {
          const lThis = this;
          const process = await exec(`"${this._ecbPath}" ${additionnalArgs} -config ${this._projectFileName}`,
               {
                    cwd: this._cwd
               }, function (error, stdout, stderr) {
                    if (error !== null || (error === null && stdout.indexOf('cannot be read') !== -1)) {
                         lThis._error = true;
                         console.log('exec error: ' + error);
                    }
               });

          let inputString = "";
          inputs.forEach(input => {
               inputString = inputString + input + "\n";
          });

          let stderrData: string = "";

          let stderrDataPromise: Promise<string>;

          process.stderr?.on('data', (data) => {
               const stringData = data.toString();
               stderrData = stderrData + stringData;
               //Write to output. 
               this._extensionOutputChannel.appendLine(stringData);
          });

          process.stdout?.on('data', (data) => {
               const stringData = data.toString();
               //Write to output.
               this._extensionOutputChannel.appendLine(stringData);
          });

          stderrDataPromise = new Promise<string>((resolve, reject) => {
               process.on('close', (code) => {
                    process.kill();
                    if (code === 0 && !this._error) {
                         resolve(stderrData);
                    } else {
                         reject("Error status code : " + code);
                    }
               });
          });
          process.stdin?.write(inputString);
          process.stdin?.end();

          return stderrDataPromise;
     }

     /**
      * Start the progress bar process
      * 
      * By default, you must manually tell the progress (in your code) that is it done 
      * by assigning the property "_progessionIndex" to the same value than "maxProgressIndex",
      * but you can also override the function for another behavior.
      * 
      * @param maxProgressIndex The maximum index of the progress bar. When this index is reached, the process is considered done.
      * @param taskName The name of the task to appear in the progress window.
      */
     protected async _startProgressBarProcess(maxProgressIndex: number, taskName: string) {
          const lThis = this;
          window.withProgress({
               location: ProgressLocation.Notification,
               title: taskName,
               cancellable: false
          }, (progress, token) => {
               token.onCancellationRequested(() => {
                    console.log("User canceled the long running operation");
               });

               const projectIndexedPromise = new Promise<void>((resolve, reject) => {
                    (function waitForCheck() {
                         if (lThis._error) {
                              reject();
                         }
                         else {
                              if (lThis._progessionIndex >= maxProgressIndex) {
                                   resolve();
                              }
                         }
                         setTimeout(waitForCheck, 1000);
                    })();
               });

               lThis._updateProgressBar(progress, maxProgressIndex);

               return projectIndexedPromise;
          });
     }

     /**
      * Update de the progress bar
      * 
      * @param progress The progress bar
      * @param maxProgressIndex The maximum index of the progress bar. When this index is reached, the process is considered done.
      */
     protected _updateProgressBar(progress: Progress<{
          message?: string | undefined;
          increment?: number | undefined;
     }>, maxProgressIndex: number): void {
          const lProgessionIndex = this._progessionIndex;
          const lMaxProgressIndex = maxProgressIndex;
          progress.report({ message: `building...` });
          this._oldProgressionIndex = lProgessionIndex;
          if (lProgessionIndex < lMaxProgressIndex) {
               setTimeout(() => {
                    this._updateProgressBar(progress, maxProgressIndex);
               }, 1000);
          }
     }

}