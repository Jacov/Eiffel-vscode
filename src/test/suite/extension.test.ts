import * as assert from 'assert';

// You can import and use all API from the 'vscode' module
// as well as import your extension to test it
import { window } from 'vscode';
import { ProjectCompiler } from '../../tools/projectCompiler';
import { ProjectIndexer } from '../../tools/projectIndexer';

window.showInformationMessage('Start all tests.');
console.log('Start all tests.');

/**
 * The eiffel compiler path
 */
let ecPath: string;

/**
 * The current working directory
 */
let cwdPath: string;

/**
 * The .ecf file name of the test project
 */
const PROJECT_FILE_NAME: string = "test_project.ecf";

/**
 * Below is the checks if environment variables have been set for the above three attributes "ecPath" and "cwdPath".
 */
// Check for Eiffel compiler path
if (process.env["EIFFEL_VSCODE_EC_PATH"]) {
	ecPath = process.env["EIFFEL_VSCODE_EC_PATH"];
} else {
	ecPath = "";
}
// Check for Eiffel project directory path
if (process.env["EIFFEL_VSCODE_TEST_CWD_PATH"]) {
	cwdPath = process.env["EIFFEL_VSCODE_TEST_CWD_PATH"];
} else {
	cwdPath = "";
}

// Tests for the "ProjectCompiler" class (projectCompiler.ts)
suite('Extension Test Suite : compiler tests', function () {
	this.timeout(300000);
	
	let projectCompiler: ProjectCompiler;

	test('Project compiler with correct paths gives no error', async () => {

		projectCompiler = new ProjectCompiler(
			ecPath,
			cwdPath,
			PROJECT_FILE_NAME);
		await projectCompiler.compileAndRun();
		assert.strictEqual(projectCompiler._error, false);
	});

	test('Project compiler with incorrect compiler path gives rejected promise', async () => {
		projectCompiler = new ProjectCompiler(
			"Incorrect Folder",
			cwdPath,
			PROJECT_FILE_NAME);
		await assert.rejects(projectCompiler.compileAndRun());
	});

	
	test('Project compiler with incorrect current working directory path gives rejected promise', async () => {
		projectCompiler = new ProjectCompiler(
			ecPath,
			"Incorrect Folder",
			PROJECT_FILE_NAME);
		await assert.rejects(projectCompiler.compileAndRun());
	});

	test('Project compiler with incorrect project file name gives rejected promise', async () => {
		projectCompiler = new ProjectCompiler(
			ecPath,
			cwdPath,
			"incorrect_file_name.ecf");
		await assert.rejects(projectCompiler.compileAndRun());
	});
});

// Tests for the "ProjectIndexer" class (projectIndexer.ts)
suite('Extension Test Suite : indexer tests', function () {
	this.timeout(1800000);

	const projectIndexer = new ProjectIndexer(
		ecPath,
		cwdPath,
		PROJECT_FILE_NAME);

	suiteSetup('Preparing test environnement. This may take few minutes.', async () => {
		await projectIndexer.indexProject();
	});

	test('Project indexer with correct paths gives no error', () => {
		assert.strictEqual(projectIndexer._error, false);
	});

	test('APPLICATION class exists and is properly fullfiled with according identifiers', () => {
		const classObjectFound = projectIndexer.classes.find((classObject) => {
			return classObject.name === 'APPLICATION';
		});
		assert.notStrictEqual(classObjectFound, undefined);
		assert.strictEqual(classObjectFound?.name, 'APPLICATION');
		assert.strictEqual(classObjectFound?.attributes.length, 8);
		assert.strictEqual(classObjectFound?.constants.length, 6);
		assert.strictEqual(classObjectFound?.routines.length, 53);
	});

	test('FOO_WITH_ALL class exists and is properly fullfiled with according identifiers', () => {
		const classObjectFound = projectIndexer.classes.find((classObject) => {
			return classObject.name === 'FOO_WITH_ALL';
		});
		assert.notStrictEqual(classObjectFound, undefined);
		assert.strictEqual(classObjectFound?.name, 'FOO_WITH_ALL');
		assert.strictEqual(classObjectFound?.attributes.length, 3);
		assert.strictEqual(classObjectFound?.constants.length, 8);
		assert.strictEqual(classObjectFound?.routines.length, 57);
	});

	test('FOO_WITH_ATTRIBUTES class exists and is properly fullfiled with according identifiers', () => {
		const classObjectFound = projectIndexer.classes.find((classObject) => {
			return classObject.name === 'FOO_WITH_ATTRIBUTES';
		});
		assert.notStrictEqual(classObjectFound, undefined);
		assert.strictEqual(classObjectFound?.name, 'FOO_WITH_ATTRIBUTES');
		assert.strictEqual(classObjectFound?.attributes.length, 3);
		assert.strictEqual(classObjectFound?.constants.length, 6);
		assert.strictEqual(classObjectFound?.routines.length, 53);
	});

	test('FOO_WITH_CONSTANTS_AND_ATTRIBUTES class exists and is properly fullfiled with according identifiers', () => {
		const classObjectFound = projectIndexer.classes.find((classObject) => {
			return classObject.name === 'FOO_WITH_CONSTANTS_AND_ATTRIBUTES';
		});
		assert.notStrictEqual(classObjectFound, undefined);
		assert.strictEqual(classObjectFound?.name, 'FOO_WITH_CONSTANTS_AND_ATTRIBUTES');
		assert.strictEqual(classObjectFound?.attributes.length, 3);
		assert.strictEqual(classObjectFound?.constants.length, 8);
		assert.strictEqual(classObjectFound?.routines.length, 53);
	});

	test('FOO_WITH_CONSTANTS class exists and is properly fullfiled with according identifiers', () => {
		const classObjectFound = projectIndexer.classes.find((classObject) => {
			return classObject.name === 'FOO_WITH_CONSTANTS';
		});
		assert.notStrictEqual(classObjectFound, undefined);
		assert.strictEqual(classObjectFound?.name, 'FOO_WITH_CONSTANTS');
		assert.strictEqual(classObjectFound?.attributes.length, 0);
		assert.strictEqual(classObjectFound?.constants.length, 8);
		assert.strictEqual(classObjectFound?.routines.length, 53);
	});

	test('FOO_WITH_ROUTINES_AND_ATTRIBUTES class exists and is properly fullfiled with according identifiers', () => {
		const classObjectFound = projectIndexer.classes.find((classObject) => {
			return classObject.name === 'FOO_WITH_ROUTINES_AND_ATTRIBUTES';
		});
		assert.notStrictEqual(classObjectFound, undefined);
		assert.strictEqual(classObjectFound?.name, 'FOO_WITH_ROUTINES_AND_ATTRIBUTES');
		assert.strictEqual(classObjectFound?.attributes.length, 3);
		assert.strictEqual(classObjectFound?.constants.length, 6);
		assert.strictEqual(classObjectFound?.routines.length, 57);
	});

	test('FOO_WITH_ROUTINES_AND_CONSTANTS class exists and is properly fullfiled with according identifiers', () => {
		const classObjectFound = projectIndexer.classes.find((classObject) => {
			return classObject.name === 'FOO_WITH_ROUTINES_AND_CONSTANTS';
		});
		assert.notStrictEqual(classObjectFound, undefined);
		assert.strictEqual(classObjectFound?.name, 'FOO_WITH_ROUTINES_AND_CONSTANTS');
		assert.strictEqual(classObjectFound?.attributes.length, 0);
		assert.strictEqual(classObjectFound?.constants.length, 8);
		assert.strictEqual(classObjectFound?.routines.length, 55);
	});

	test('FOO_WITH_ROUTINES class exists and is properly fullfiled with according identifiers', () => {
		const classObjectFound = projectIndexer.classes.find((classObject) => {
			return classObject.name === 'FOO_WITH_ROUTINES';
		});
		assert.notStrictEqual(classObjectFound, undefined);
		assert.strictEqual(classObjectFound?.name, 'FOO_WITH_ROUTINES');
		assert.strictEqual(classObjectFound?.attributes.length, 0);
		assert.strictEqual(classObjectFound?.constants.length, 6);
		assert.strictEqual(classObjectFound?.routines.length, 55);
	});

	test('FOO_WITHOUT_IDENTIFIERS class exists and is properly fullfiled with according identifiers', () => {
		const classObjectFound = projectIndexer.classes.find((classObject) => {
			return classObject.name === 'FOO_WITHOUT_IDENTIFIERS';
		});
		assert.notStrictEqual(classObjectFound, undefined);
		assert.strictEqual(classObjectFound?.name, 'FOO_WITHOUT_IDENTIFIERS');
		assert.strictEqual(classObjectFound?.attributes.length, 0);
		assert.strictEqual(classObjectFound?.constants.length, 6);
		assert.strictEqual(classObjectFound?.routines.length, 53);
	});
});
