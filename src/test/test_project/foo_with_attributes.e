note
	description: "foo class"
	date: "$Date$"
	revision: "$Revision$"

class
	FOO_WITH_ATTRIBUTES

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make(a_foo_name: STRING; a_foo_x, a_foo_y: INTEGER)
		do
            foo_x := a_foo_x
            foo_y := a_foo_y
            foo_name := a_foo_name
			print ("Foo with attributes is working!%N")
        ensure
            foo_x = a_foo_x
            foo_y = a_foo_y
            foo_name = a_foo_name
		end

feature -- Attributes

    foo_name: STRING

    foo_x: INTEGER

    foo_y: INTEGER

end
