note
	description: "foo class"
	date: "$Date$"
	revision: "$Revision$"

class
	FOO_WITHOUT_IDENTIFIERS

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
		do
			print ("Foo without identifiers is working!%N")
		end

end
