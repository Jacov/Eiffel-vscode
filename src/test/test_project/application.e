note
	description: "test_project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	foo_with_all: FOO_WITH_ALL

	foo_with_attributes: FOO_WITH_ATTRIBUTES

	foo_with_constants_and_attributes: FOO_WITH_CONSTANTS_AND_ATTRIBUTES

	foo_with_constant: FOO_WITH_CONSTANTS

	foo_with_routines_and_attributes: FOO_WITH_ROUTINES_AND_ATTRIBUTES

	foo_with_routines_and_constants: FOO_WITH_ROUTINES_AND_CONSTANTS

	foo_with_routines: FOO_WITH_ROUTINES

	foo_without_identifiers: FOO_WITHOUT_IDENTIFIERS

	make
		do
			create foo_with_all.make("Foo's with all name.", 1, 2)
			create foo_with_attributes.make("Foo's with all name.", 1, 2)
			create foo_with_constants_and_attributes.make("Foo's with all name.", 1, 2)
			create foo_with_constant.make
			create foo_with_routines_and_attributes.make("Foo's with all name.", 1, 2)
			create foo_with_routines_and_constants.make
			create foo_with_routines.make
			create foo_without_identifiers.make
			print ("Hello Eiffel World!%N")
		end

end
